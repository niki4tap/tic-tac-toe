#include "board.cpp"
#include "random.hpp"
#include <iostream>
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

int end() {
    while (true) {
        char choice{' '};
        std::string fake_choice{"  "};
        bool yes_flag{false};
        while (!yes_flag) {
            std::cout << "Want to play again? (y/n): ";
            std::cin >> fake_choice;
            choice = fake_choice[0];
            std::cout << std::endl;
            switch (choice) {
                case 'y':
                    return 0;
        
                case 'n':
                    return 1;

                default:
                    break;

            }
        }
    }
}

char ask_player() {
    while (true) {
        std::string fake_player_char{" "};
        printf("Choose a character (X, O, (r)andom):");
        std::cin >> fake_player_char;
        char player_char = fake_player_char[0];
        switch (player_char) {
            case 'X':
                return 'X';

            case 'O':
                return 'O';

            case 'r':
                int random{0};
                random = effolkronium::random_static::get(1,2);
                if (random == 1) {
                    return 'X';
                } else {
                    return 'O';
                }
        }
    }
}

void game() {
    Board game_board;
    game_board.nullify();
    char player_char = ask_player();
    printf("You have chosen: %c\n", player_char);
    game_board.current_char = 'O';
    while (game_board.check_win() == ' ') {
        printf("Current board:\n");
        game_board.print_board();
        std::cout << std::endl;
        if (game_board.current_char == player_char) {
            while (true) {
                std::string fake_place;
                int place;
                printf("Where to put '%c' [1-9]: ", game_board.current_char);
                std::cin >> fake_place;
                place = fake_place[0] - '0';
                if (place <= 9 and place >= 1) {
                    int is_ok = game_board.make_move(place);
                    if (is_ok != 1) {
                        break;
                    }
                }
            }
        } else {
            printf("AI is making a move...\n");
            #ifdef _WIN32
            Sleep(2000);
            #else
            sleep(2);
            #endif
            while (true) {
                int random{0};
                random = effolkronium::random_static::get(1,9);
                int is_ok = game_board.make_move(random);
                if (is_ok != 1) {
                    printf("AI moved %c to %i\n", game_board.current_char, random);
                    break;
                }
            }
        }
    }
    game_board.print_board();
    if (game_board.check_win() != 'T') {
        printf("'%c' won!\n", game_board.current_char);
    } else {
        printf("A tie!\n");
    }
}

int main() {
    while (true) {
        game();
        int if_ended;
        if_ended = end();
        if (if_ended == 1) {
            return 0;
        }
    }
}