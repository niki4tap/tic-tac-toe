#!/bin/bash

g++ ./main.cpp -o ./tic-tac-toe
if [ $? -eq 0 ] 
then
    chmod +x ./tic-tac-toe
fi

x86_64-w64-mingw32-g++ -static-libstdc++ -static-libgcc ./main.cpp -o ./tic-tac-toe_win32.exe

i686-w64-mingw32-g++-win32 -static-libstdc++ -static-libgcc ./main.cpp -o ./tic-tac-toe_win64.exe