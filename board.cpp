#include <iostream>

class Board
{
private:
    char board_list[9];
    
    int check_tie(char board_list[]) {
        for (int i{0}; i < 8; i++) {
            if (board_list[i] == ' ') {
                return 1;
            }
        }
        
        return 0;
    }

public:
    char current_char{'X'};

    void nullify() {
        for (int i=0;i<=8;i++){
            board_list[i] = ' ';
        }
    }

    void print_board() {
        #ifdef _WIN32
        printf(
            "/-----------\\\n"
            "| %c | %c | %c |\n"
            "|---+---+---|\n"
            "| %c | %c | %c |\n"
            "|---+---+---|\n"
            "| %c | %c | %c |\n"
            "\\-----------/\n",
            board_list[0],
            board_list[1],
            board_list[2],
            board_list[3],
            board_list[4],
            board_list[5],
            board_list[6],
            board_list[7],
            board_list[8]
        );
        #else
        printf(
            "┌───┬───┬───┐\n"
            "│ %c │ %c │ %c │\n"
            "├───┼───┼───┤\n"
            "│ %c │ %c │ %c │\n"
            "├───┼───┼───┤\n"
            "│ %c │ %c │ %c │\n"
            "└───┴───┴───┘\n",
            board_list[0],
            board_list[1],
            board_list[2],
            board_list[3],
            board_list[4],
            board_list[5],
            board_list[6],
            board_list[7],
            board_list[8]
        );
        #endif
    }

    int make_move(int place) {
        if (board_list[place-1] != ' ') { return 1; }
        switch (current_char) {
            case 'X':
                board_list[place-1] = current_char;
                return 0;
            case 'O':
                board_list[place-1] = current_char;
                return 0;
            default:
                return 1;
        }
    }

    // Checks for win, and returns char that won the game.
    char check_win() {
        // Check for horizontal win:
        for (int i=0;i<=8;i+=3) {
            if (board_list[i] == current_char and board_list[i+1] == current_char and board_list[i+2] == current_char) {
                return current_char;
            }
        }
        // Check for vertical win:
        for (int i=0;i<=2;i++) {
            if (board_list[i] == current_char and board_list[i+3] == current_char and board_list[i+6] == current_char) {
                return current_char;
            }
        }
        // Check for other conditions:
        if (board_list[0] == current_char and board_list[4] == current_char and board_list[8] == current_char) {
            return current_char;
        }

        if (board_list[2] == current_char and board_list[4] == current_char and board_list[6] == current_char) {
            return current_char;
        }

        switch (current_char) {
        
        case 'X':
            current_char = 'O';
            break;
        
        case 'O':
            current_char = 'X';
            break;
        }

        // Check for a tie:
        int res = check_tie(board_list);
        if (res == 0) {
            return 'T';
        }

        return ' ';
    }
};